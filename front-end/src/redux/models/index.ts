export interface OderState {
    oder?: OderModel;
    error: string | undefined;
}

export interface OderModel {
    checkoutUrl: string;
    expires: string;
    token: string;
}

interface Amount {
    amount: string;
    currency: string;
};

interface Consumer {
    phoneNumber: string;
    givenNames: string;
    surname: string;
    email: string;
};

interface Address {
    name: string;
    line1: string;
    suburb: string;
    postcode: string;
    countryCode: string;
    phoneNumber: string;
};

interface Price {
    amount: string;
    currency: string;
};

interface Item {
    name: string;
    category: string;
    subcategory: string[];
    brand: string;
    gtin: string;
    sku: string;
    quantity: number;
    price: Price;
    pageUrl?: string;
    imageUrl?: string;
};

interface Discount {
    displayName: string;
    amount: Price;
};

interface Merchant {
    redirectConfirmUrl: string;
    redirectCancelUrl: string;
};

interface Frequency {
    number: string;
    frequencyType: string;
};

export interface OrderCreateRequest {
    totalAmount: Amount;
    consumer: Consumer;
    billing: Address;
    shipping: Address;
    items: Item[];
    discounts: Discount[];
    merchant: Merchant;
    merchantReference: string;
    taxAmount: Amount;
    shippingAmount: Amount;
    interface: string;
    product: string;
    type: string;
    frequency: Frequency;
    orderExpiryMilliseconds: number;
};
