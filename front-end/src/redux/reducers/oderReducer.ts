import { OrderActionType } from "../action-types";
import { OrderActions } from "../actions/oderActions";
import { OderModel, OderState } from "../models";


const initialState: OderState = {
    oder: undefined,
    error: undefined
}

const oderReducer = (state: OderState = initialState, action: OrderActions) => {
    switch (action.type) {
        case OrderActionType.CREATE_ORDER:
            return {
                ...state,
                oder: action.payload
            }
        default:
            return state;

    }
}

export default oderReducer;