import { combineReducers } from 'redux';
import oderReducer from './oderReducer';

const rootReducer = combineReducers({
   oderReducer,
});

export default rootReducer;

export type State = ReturnType<typeof rootReducer>