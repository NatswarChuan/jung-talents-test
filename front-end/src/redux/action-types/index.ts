export enum OrderActionType {
    CREATE_ORDER = 'CREATE_ORDER',
    ON_ORDER_ERROR = 'ON_ORDER_ERROR'
}