import { Dispatch } from "redux";
import axios from 'axios';
import { OrderActionType } from "../action-types";
import { server } from "../../consts/Selector";
import { OderModel, OrderCreateRequest } from "../models";

enum URI_ORDER {
    CREATE_ORDER = "/v2/orders"
}

export interface AddOrder {
    readonly type: OrderActionType.CREATE_ORDER,
    payload: OderModel,
}

export type OrderActions = AddOrder;

export const createOrder = (data: OrderCreateRequest) => {
    return async (dispatch: Dispatch<OrderActions>) => {
        axios.post(`${server}${URI_ORDER.CREATE_ORDER}`, data).then((response: any) => {
            dispatch({
                type: OrderActionType.CREATE_ORDER,
                payload: response.data
            })
        }).catch((error: any) => {
            if (error.response.status === 400) {
                let message = "";
                error.response.data.data.forEach((element:any) => {
                    message += `${element.field} ${element.message}\n`;
                });
                alert(message);
            }
        });
    }
}
