import { useState,useEffect  } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { OderState, OrderCreateRequest, State, createOrder } from '../redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


export default function Home() {
  const initialFormData: OrderCreateRequest = {
    "totalAmount": {
      "amount": "190.00",
      "currency": "EUR"
    },
    "consumer": {
      "phoneNumber": "0400000001",
      "givenNames": "Joe",
      "surname": "Consumer",
      "email": "test@scalapay.com"
    },
    "billing": {
      "name": "Joe Consumer",
      "line1": "Via della Rosa, 58",
      "suburb": "Montelupo Fiorentino",
      "postcode": "50056",
      "countryCode": "IT",
      "phoneNumber": "0400000000"
    },
    "shipping": {
      "name": "Joe Consumer",
      "line1": "Via della Rosa, 58",
      "suburb": "Montelupo Fiorentino",
      "postcode": "50056",
      "countryCode": "IT",
      "phoneNumber": "0400000000"
    },
    "items": [
      {
        "name": "T-Shirt",
        "category": "clothes",
        "subcategory": [
          "shirt",
          "long-sleeve"
        ],
        "brand": "TopChoice",
        "gtin": "123458791330",
        "sku": "12341234",
        "quantity": 1,
        "price": {
          "amount": "10.00",
          "currency": "EUR"
        },
        "pageUrl": "https://www.scalapay.com//product/view/",
        "imageUrl": "https://www.scalapay.com//product/view/"
      },
      {
        "name": "Jeans",
        "category": "clothes",
        "subcategory": [
          "pants",
          "jeans"
        ],
        "brand": "TopChoice",
        "gtin": "123458722222",
        "sku": "12341235",
        "quantity": 1,
        "price": {
          "amount": "20.00",
          "currency": "EUR"
        }
      }
    ],
    "discounts": [
      {
        "displayName": "10% Off",
        "amount": {
          "amount": "3.00",
          "currency": "EUR"
        }
      }
    ],
    "merchant": {
      "redirectConfirmUrl": "https://portal.integration.scalapay.com/success-url",
      "redirectCancelUrl": "https://portal.integration.scalapay.com/failure-url"
    },
    "merchantReference": "merchantOrder-1234",
    "taxAmount": {
      "amount": "3.70",
      "currency": "EUR"
    },
    "shippingAmount": {
      "amount": "10.00",
      "currency": "EUR"
    },
    "type": "online",
    "product": "pay-in-3",
    "frequency": {
      "number": "1",
      "frequencyType": "monthly"
    },
    "orderExpiryMilliseconds": 600000,
    interface: ''
  };

  const orderState: OderState = useSelector((state: State) => state.oderReducer);
  const dispatch = useDispatch();
  const [formData, setFormData] = useState(initialFormData);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    console.log(formData);
    dispatch(createOrder(formData));
  };

  
  useEffect(() => {
    console.log(orderState.oder);
    if (!!orderState.oder) {
      window.location.href = orderState.oder.checkoutUrl;
    }
  }, [orderState])
  
  const handleChange = (e: any) => {
    const { name, value } = e.target;
  
    // Tạo bản sao mới của formData
    const updatedFormData:any = { ...formData };
  
    // Thực hiện cập nhật sâu hơn
    const nameParts = name.split('.');
    if (nameParts.length === 2) {
      updatedFormData[nameParts[0]] = { ...updatedFormData[nameParts[0]] };
      updatedFormData[nameParts[0]][nameParts[1]] = value;
    } else {
      updatedFormData[name] = value;
    }
  
    // Cập nhật state với bản sao mới
    setFormData(updatedFormData);
  };

  return (
    <Form onSubmit={handleSubmit}>
      {/* Total Amount */}
      <>
        <Form.Group>
          <Form.Label>Total Amount</Form.Label>
          <Form.Control
            type="text"
            name="totalAmount.amount"
            value={formData.totalAmount.amount}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Currency</Form.Label>
          <Form.Control
            type="text"
            name="totalAmount.currency"
            value={formData.totalAmount.currency}
            onChange={handleChange}
          />
        </Form.Group>
      </>
      {/* Consumer */}
      <>
        <Form.Group>
          <Form.Label>Consumer Phone Number</Form.Label>
          <Form.Control
            type="text"
            name="consumer.phoneNumber"
            value={formData.consumer.phoneNumber}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Given Names</Form.Label>
          <Form.Control
            type="text"
            name="consumer.givenNames"
            value={formData.consumer.givenNames}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Surname</Form.Label>
          <Form.Control
            type="text"
            name="consumer.surname"
            value={formData.consumer.surname}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            name="consumer.email"
            value={formData.consumer.email}
            onChange={handleChange}
          />
        </Form.Group>
      </>
      {/* Billing Address */}
      <>
        <Form.Group>
          <Form.Label>Billing Name</Form.Label>
          <Form.Control
            type="text"
            name="billing.name"
            value={formData.billing.name}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Line 1</Form.Label>
          <Form.Control
            type="text"
            name="billing.line1"
            value={formData.billing.line1}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Suburb</Form.Label>
          <Form.Control
            type="text"
            name="billing.suburb"
            value={formData.billing.suburb}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Postcode</Form.Label>
          <Form.Control
            type="text"
            name="billing.postcode"
            value={formData.billing.postcode}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Country Code</Form.Label>
          <Form.Control
            type="text"
            name="billing.countryCode"
            value={formData.billing.countryCode}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            type="text"
            name="billing.phoneNumber"
            value={formData.billing.phoneNumber}
            onChange={handleChange}
          />
        </Form.Group>
      </>
      {/* Shipping Address */}
      <>
        <Form.Group>
          <Form.Label>Shipping Name</Form.Label>
          <Form.Control
            type="text"
            name="shipping.name"
            value={formData.shipping.name}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Line 1</Form.Label>
          <Form.Control
            type="text"
            name="shipping.line1"
            value={formData.shipping.line1}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Suburb</Form.Label>
          <Form.Control
            type="text"
            name="shipping.suburb"
            value={formData.shipping.suburb}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Postcode</Form.Label>
          <Form.Control
            type="text"
            name="shipping.postcode"
            value={formData.shipping.postcode}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Country Code</Form.Label>
          <Form.Control
            type="text"
            name="shipping.countryCode"
            value={formData.shipping.countryCode}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            type="text"
            name="shipping.phoneNumber"
            value={formData.shipping.phoneNumber}
            onChange={handleChange}
          />
        </Form.Group>
      </>
      <h1 className="text-center">Items</h1>
      {/* Items */}
      <>
        {formData.items.map((item, index) => (
          <div key={index}>
            <Form.Group>
              <Form.Label>Item Name</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].name`}
                value={item.name}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].category`}
                value={item.category}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Subcategory</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].subcategory`}
                value={item.subcategory}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Brand</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].brand`}
                value={item.brand}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>GTIN</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].gtin`}
                value={item.gtin}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>SKU</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].sku`}
                value={item.sku}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].quantity`}
                value={item.quantity}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price Amount</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].price.amount`}
                value={item.price.amount}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price Currency</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].price.currency`}
                value={item.price.currency}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Page URL</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].pageUrl`}
                value={item.pageUrl}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Image URL</Form.Label>
              <Form.Control
                type="text"
                name={`items[${index}].imageUrl`}
                value={item.imageUrl}
                onChange={handleChange}
              />
            </Form.Group>
          </div>
        ))}

        {/* Add Item Button */}
        <Button
          variant="primary"
          onClick={() => {
            setFormData({
              ...formData,
              items: [
                ...formData.items,
                {
                  name: '',
                  category: '',
                  subcategory: [],
                  brand: '',
                  gtin: '',
                  sku: '',
                  quantity: 0,
                  price: {
                    amount: '',
                    currency: ''
                  },
                  pageUrl: '',
                  imageUrl: ''
                }
              ]
            });
          }}
        >
          Add Item
        </Button>
      </>
      <h1 className="text-center">End Items</h1>
      <h1 className="text-center">Discounts</h1>
      {/* Discounts */}
      <>
        {formData.discounts.map((item, index) => (
          <div key={index}>
            <Form.Group>
              <Form.Label>Discounts Display Name</Form.Label>
              <Form.Control
                type="text"
                name={`discounts[${index}].displayName`}
                value={item.displayName}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Discounts Amount</Form.Label>
              <Form.Control
                type="text"
                name={`discounts[${index}].amount.amount`}
                value={item.amount.amount}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Discounts Amount Currency</Form.Label>
              <Form.Control
                type="text"
                name={`discounts[${index}].amount.currency`}
                value={item.amount.currency}
                onChange={handleChange}
              />
            </Form.Group>
          </div>
        ))}

        {/* Add Discount Button */}
        <Button
          variant="primary"
          onClick={() => {
            setFormData({
              ...formData,
              discounts: [
                ...formData.discounts,
                {
                  displayName: "",
                  amount: {
                    amount: "",
                    currency: "EUR"
                  }
                }
              ]
            });
          }}
        >
          Add Discount
        </Button>
      </>
      <h1 className="text-center">End Discounts</h1>
      {/* Merchant */}
      <>
        <Form.Group>
          <Form.Label>Merchant Redirect Confirm Url</Form.Label>
          <Form.Control
            type="text"
            name="merchant.redirectConfirmUrl"
            value={formData.merchant.redirectConfirmUrl}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Merchant RedirectCancelUrl</Form.Label>
          <Form.Control
            type="text"
            name="merchant.redirectCancelUrl"
            value={formData.merchant.redirectCancelUrl}
            onChange={handleChange}
          />
        </Form.Group>
      </>

      {/* Merchant Reference */}
      <Form.Group>
        <Form.Label>Merchant Reference</Form.Label>
        <Form.Control
          type="text"
          name="merchantReference"
          value={formData.merchantReference}
          onChange={handleChange}
        />
      </Form.Group>

      {/* Tax Amount */}
      <>
        <Form.Group>
          <Form.Label>Tax Amount</Form.Label>
          <Form.Control
            type="text"
            name="taxAmount.amount"
            value={formData.taxAmount.amount}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Tax Amount Currency</Form.Label>
          <Form.Control
            type="text"
            name="taxAmount.currency"
            value={formData.taxAmount.currency}
            onChange={handleChange}
          />
        </Form.Group>
      </>

      {/* Shipping Amount */}
      <>
        <Form.Group>
          <Form.Label>Shipping Amount</Form.Label>
          <Form.Control
            type="text"
            name="shippingAmount.amount"
            value={formData.shippingAmount.amount}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Shipping Amount Currency</Form.Label>
          <Form.Control
            type="text"
            name="taxAmount.currency"
            value={formData.shippingAmount.currency}
            onChange={handleChange}
          />
        </Form.Group>
      </>

      {/* Type */}
      <Form.Group>
        <Form.Label>Type</Form.Label>
        <Form.Control
          type="text"
          name="type"
          value={formData.type}
          onChange={handleChange}
        />
      </Form.Group>

      {/* Product */}
      <Form.Group>
        <Form.Label>Product</Form.Label>
        <Form.Control
          type="text"
          name="product"
          value={formData.product}
          onChange={handleChange}
        />
      </Form.Group>

      {/* Frequency */}
      <>
        <Form.Group>
          <Form.Label>Frequency number</Form.Label>
          <Form.Control
            type="text"
            name="frequency.number"
            value={formData.shippingAmount.amount}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Frequency Type</Form.Label>
          <Form.Control
            type="text"
            name="frequency.frequencyType"
            value={formData.frequency.frequencyType}
            onChange={handleChange}
          />
        </Form.Group>
      </>

      {/* Order Expiry Milliseconds */}
      <Form.Group>
        <Form.Label>Order Expiry Milliseconds</Form.Label>
        <Form.Control
          type="text"
          name="orderExpiryMilliseconds"
          value={formData.orderExpiryMilliseconds}
          onChange={handleChange}
        />
      </Form.Group>

      {/* Frequency */}

      <Button
        variant="primary" onClick={(e: any) => handleSubmit(e)}>Submit</Button>
    </Form>
  );
};