Hướng dẫn chạy project

Project này bao gồm hai project:

    Project front-end sử dụng framework React
    Project back-end sử dụng framework Spring Boot

Cài đặt môi trường
Để chạy project, bạn cần cài đặt các phần mềm sau:

    Node.js
    JDK 17.0.7
    Gradle
    Lombok

Chạy project front-end
Để chạy project front-end, bạn thực hiện các bước sau:

    1.  Mở terminal và chuyển đến thư mục front-end
    2.  Chạy lệnh sau để cài đặt các dependency: npm install
    3.  Chạy lệnh sau để khởi động server: npm start

Project front-end sẽ được khởi động tại địa chỉ http://localhost:3000

Chạy project back-end
Để chạy project back-end, bạn thực hiện các bước sau:

    1.  Mở terminal và chuyển đến thư mục back-end
    2.  Chạy lệnh sau để cài đặt các dependency: gradle build
    3.  Chạy lệnh sau để khởi động server:  gradle bootRun

Project back-end sẽ được khởi động tại địa chỉ http://localhost:8080
