package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.commond.HttpException;
import com.example.demo.commond.endPoints.OderEndPoint;
import com.example.demo.commond.exceptionMessage.OrderExceptionMessage;
import com.example.demo.dtos.requests.OrderRequest;
import com.example.demo.dtos.responses.ScalapayResponse;

import jakarta.validation.Valid;

/**
 * Controller for handling Scalapay order creation.
 */
@RestController
@RequestMapping("v2")
public class ScalapayController {

    @Value("${scalapay.api.base-url}")
    private String scalapayBaseUrl;

    @Value("${scalapay.api.key}")
    private String scalapayApiKey;

    @Autowired
    RestTemplate restTemplate;

     /**
     * Creates a new Scalapay order based on the provided order request.
     *
     * @param orderRequest The request body containing order details.
     * @return A ResponseEntity containing the Scalapay response.
     * @throws HttpException if the order creation fails.
     */
    @PostMapping("/orders")
    public ResponseEntity<ScalapayResponse> createOrder(@RequestBody @Valid OrderRequest orderRequest) throws HttpException {
        HttpHeaders headers = new HttpHeaders();
        String urlRequest = scalapayBaseUrl+OderEndPoint.CREATE_ORDER;
        headers.set("Authorization", "Bearer " + scalapayApiKey);
        headers.setContentType(MediaType.APPLICATION_JSON); 
        HttpEntity<OrderRequest> requestEntity = new HttpEntity<>(orderRequest, headers);

        ResponseEntity<ScalapayResponse> responseEntity = restTemplate.postForEntity(urlRequest, requestEntity,
                ScalapayResponse.class);

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OrderExceptionMessage.CREATED_FAIL);
        }
        ScalapayResponse scalapayResponse = responseEntity.getBody();
        String checkoutUrl = scalapayResponse.getCheckoutUrl();

        if (checkoutUrl != null) {
            return ResponseEntity.ok(scalapayResponse);
        } else {
             throw new HttpException(HttpStatus.BAD_REQUEST, OrderExceptionMessage.CREATED_FAIL);
        }
    }
}
