package com.example.demo.dtos.requests;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.util.List;

import lombok.Data;

@Data
public class OrderRequest {

    @Valid
    @NotNull
    private TotalAmount totalAmount;

    @Valid
    @NotNull
    private Consumer consumer;

    @Valid
    @NotNull
    private Billing billing;

    @Valid
    @NotNull
    private Shipping shipping;

    @Valid
    @Size(min = 1)
    private List<Item> items;

    @Valid
    private List<Discount> discounts;

    @Valid
    @NotNull
    private Merchant merchant;

    @NotBlank
    private String merchantReference;

    @Valid
    @NotNull
    private TotalAmount taxAmount;

    @Valid
    @NotNull
    private TotalAmount shippingAmount;

    @NotBlank
    private String type;

    @NotBlank
    private String product;

    @Valid
    @NotNull
    private Frequency frequency;

    @NotNull
    private long orderExpiryMilliseconds;

}
