package com.example.demo.dtos.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class Shipping {
    @NotEmpty
    private String name;

    @NotEmpty
    private String line1;

    @NotEmpty
    private String suburb;

    @NotEmpty
    private String postcode;

    @NotEmpty
    private String countryCode;

    @NotEmpty
    @Pattern(regexp = "^(\\+\\d{1,3}[- ]?)?\\d{10,11}$")
    private String phoneNumber;
}
