package com.example.demo.dtos.requests;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class Frequency {
    @NotEmpty
    private String number;

    @NotEmpty
    private String frequencyType;
}