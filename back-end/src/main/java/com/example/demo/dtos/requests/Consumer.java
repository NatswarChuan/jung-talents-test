package com.example.demo.dtos.requests;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class Consumer {
    @NotEmpty
    @Pattern(regexp = "^(\\+\\d{1,3}[- ]?)?\\d{10,11}$")
    private String phoneNumber;

    @NotEmpty
    private String givenNames;

    @NotEmpty
    private String surname;

    @NotEmpty
    @Email
    private String email;
}
