package com.example.demo.dtos.requests;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
    public class Item {
        @NotEmpty
        private String name;
    
        @NotEmpty
        private String category;
    
        @Size(min = 1)
        private List<String> subcategory;
    
        @NotEmpty
        private String brand;
    
        @NotEmpty
        private String gtin;
    
        @NotEmpty
        private String sku;
    
        @NotNull
        private int quantity;
    
        @Valid
        @NotNull
        private TotalAmount price;
    
        private String pageUrl;
    
        private String imageUrl;
    }
    