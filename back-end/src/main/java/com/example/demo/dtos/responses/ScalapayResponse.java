package com.example.demo.dtos.responses;


import lombok.Data;

@Data
public class ScalapayResponse {
    String checkoutUrl;
    String expires;
    String token;
}
