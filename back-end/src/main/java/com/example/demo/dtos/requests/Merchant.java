package com.example.demo.dtos.requests;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class Merchant {
    @NotEmpty
    private String redirectConfirmUrl;

    @NotEmpty
    private String redirectCancelUrl;
}