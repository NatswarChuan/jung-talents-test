package com.example.demo.dtos.requests;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Discount {
    @NotEmpty
    private String displayName;

    @Valid
    @NotNull
    private TotalAmount amount;
}